//
//  AppDelegate.h
//  RAC
//
//  Created by 赵文超 on 2018/10/19.
//  Copyright © 2018 赵文超. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

