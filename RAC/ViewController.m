//
//  ViewController.m
//  RAC
//
//  Created by 赵文超 on 2018/10/19.
//  Copyright © 2018 赵文超. All rights reserved.
//

#import "ViewController.h"
//#import <ReactiveObjC.h>
//#import "SVGKit.h"
//#import <WebKit/WebKit.h>

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *userName;
@property (weak, nonatomic) IBOutlet UITextField *psw;
@property (strong, nonatomic) IBOutlet UIView *loginButton;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    NSString * sr = [NSString new];
    //斐波那契数列
    NSLog(@"走法：%@", @([self calculate:5]));
    NSLog(@"走法：%@", @([self calculate:4]));
    NSLog(@"走法：%@", @([self calculate:3]));
    // Do any additional setup after loading the view, typically from a nib
}

- (NSInteger)calculate:(NSInteger)number {
    if (number == 0) {
        return 0;
    } else if (number == 1) {
        return 1;
    } else if (number == 2) {
        return 2;
    } else {
        return [self calculate:number - 1] + [self calculate:number - 2];
    }
}

@end
